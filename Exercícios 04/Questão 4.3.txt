I & P => [S] I

I = papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers)

addpaper
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : 1..163 & card(papers) < 60 =>
        [papers := papers \/ {hh}] (papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers))
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : 1..163 & card(papers) < 60 =>
        (papers \/ {hh} <: 1..163 & magazines <: papers \/ {hh} & card(papers \/ {hh}) <= 60 & 2 * card(magazines) <= card(papers \/ {hh}))
    ->
        1. Como papers <: 1..163 & hh : 1..163, então temos que: papers \/ {hh} <: 1..163
        2. Como magazines <: papers, temos que magazines <: papers \/ A, seja A qualquer conjunto, inclusive A = {hh}
        3. Como tempos que card(papers) < 60, caso hh : papers, então card(papers) = card(papers \/ {hh}) < 60, caso contrário, card(papers \/ {hh}) < 61, ou <= 60
        4. Como temos que 2 * card(magazines) <= card(papers), então temos os seguintes casos, hh : papers e hh /: papers
            4.1 hh : papers, então card(papers \/ {hh}) = card(papers), consequentemente, 2 * card(magazines) <= card(papers) = card(papers \/ {hh})
            4.2 hh /: papers, então card(papers) < card(papers \/ {hh}). Portanto, 2 * card(magazines) <= card(papers) < card(papers \/ {hh}).
                Logo, 2 * card(magazines) < card(papers \/ {hh}). Mas se x < y, temos que x < y || x = y, consequentemente, x <= y.
        
    -> Por 1, 2, 3 e 4 temos que
        papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : 1..163 & card(papers) < 60 =>
        true & true & true & true = true
    
addmagazine
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : papers =>
        [magazines := magazines \/ {hh}] (papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers))
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : papers =>
        (papers <: 1..163 & magazines \/ {hh} <: papers & card(papers) <= 60 & 2 * card(magazines \/ {hh}) <= card(papers))
    ->
        1. papers foi inalterado, logo, papers <: 1..163
        2. Como hh : papers & magazines <: papers, então temos que magazines \/ {hh} <: papers
        3. Suponha o seguinte cenário:
            papers = {1, 2}, magazines = {1}, card(papers) = 2 <= 60, 2 * card(magazines) = 2 <= card(papers) = 2,
            mas caso hh = 2, então teremos que 2 * card(magazines \/ {2}) = 4 e card(papers) = 2, mas not(4 <= 2)
    -> Por 1, 2 e 3 temos que 
        papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : papers =>
        true & true & false = false

remove
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : 1..163 =>
        [papers := papers - {hh} || magazines := magazines - {hh}] (papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers))
    -> papers <: 1..163 & magazines <: papers & card(papers) <= 60 & 2 * card(magazines) <= card(papers) & hh : 1..163 =>
        (papers - {hh} <: 1..163 & magazines - {hh} <: papers - {hh} & card(papers - {hh}) <= 60 & 2 * card(magazines - {hh}) <= card(papers - {hh}))
    -> 
        1. Como hh : 1..163 & papers <: 1..163, então temos que papers - {hh} <: 1..163
        2. Como, magazines <: papers & hh : 1..163, então temos que magazines - {hh} <: papers - {hh}
        3. Como hh : 1..163 & card(papers) <= 60, então temos que card(papers - {hh}) < 60, ou ainda card(papers - {hh}) <= 60
        4. Como hh : 1..163 & 2 * card(magazines) <= card(papers)
            4.1 hh : magazines
                hh : magazines => 2 * card(magazines - {hh}) < 2 * card(magazines)
                Com isso, temos que 2 * card(magazines - {hh}) < 2 * card(magazines)
                Também, card(papers - {hh}) < card(papers)

            4.2